const initialState = {
    value: '',
    isRight: '',
    trueCombination: '1234',
    answer: null
};


const reducer = (state = initialState, action) => {
    if (action.type === 'ADD_NUMBER' &&  state.value.length < 4 ) {
        return {
            ...state,
            value: state.value += action.num
        }
    }

    if(action.type === 'REMOVE_NUMBER') {
        const string = state.value.substr(0, state.value.length - 1);
        return {
            ...state,
            value: string,
            isRight: 'white',
            answer: null
        };
    }

    if(action.type === 'CHECK_COMBINATION') {
        if(state.trueCombination === state.value) {
            return {...state, isRight: 'access', answer: 'Access Granted'}
        } else {
            return {...state, isRight: 'error', answer: 'Wrong password' }
        }
    }
      return state

};



export default reducer;