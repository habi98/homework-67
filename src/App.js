import React, { Component } from 'react';
import './App.css';
import FormSimulator from "./containers/FormSimulator/FormSimulator";

class App extends Component {
  render() {
    return (
            <FormSimulator/>
    );
  }
}

export default App;
