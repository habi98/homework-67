import React, {Component} from 'react';
import {connect} from 'react-redux'

import './FormSimulator.css'
const number = ['7', '8', '9','4','5', '6',  '1', '2', '3', '0'];

class FormSimulator extends Component {

    render() {
        return (
            <div className="simulator">
                <div className="div-number ">
                    {this.props.answer ?<div className={this.props.answer}>{this.props.answer}</div> : null}
                    <form>
                        <input type="password" value={this.props.value} onChange={this.props.valueChange} className={"input ".concat(this.props.isRight)}/>
                    </form>
                    {number.map((number, index) => (
                        <span key={index}><button onClick={()=> this.props.addNumber(number)}>{number}</button></span>
                    ))}
                    <span><button onClick={this.props.removedNumber}> c </button></span>
                    <span><button onClick={this.props.check}> E </button></span>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        value: state.value,
        isRight: state.isRight,
        answer: state.answer
    }
};

const mapDispatchToProps = dispatch => {
    return {
      addNumber: num => dispatch({type: 'ADD_NUMBER', num, }),
      removedNumber: () => dispatch({type: 'REMOVE_NUMBER'}),
      valueChange: event => dispatch({type: 'VALUE_CHENGE', event}),
      check: () => dispatch({type: 'CHECK_COMBINATION'})
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(FormSimulator);